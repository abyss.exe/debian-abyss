# My Debian Config!
![screenshot](https://gitlab.com/pastelabyss/debian-abyss/-/raw/main/screenshot.png)

## How to use this repo
This repo is for achieving my desktop setup easily in debian. If you want to use this script, just type in the command below on a minimal debian bookworm install:
```
bash -c "$(wget https://gitlab.com/pastelabyss/debian-abyss/-/raw/main/install -O -)"
```
